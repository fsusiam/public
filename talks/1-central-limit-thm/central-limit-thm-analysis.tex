% This work by Omid Khanmohamadi, Diego Hernan Diaz Martinez, Tony
% Wills, and Kouadio David Yao, is licensed under the Creative Commons
% Attribution-ShareAlike 4.0 International License. To view a copy of
% this license, visit
% http://creativecommons.org/licenses/by-sa/4.0/deed.en_US.

% For consistency and for compatibility with different parsers, use lines
% that are no more than 80 characters in length by setting your editor to
% do a hard wrap at 80 characters. For example, in Emacs M-x
% set-fill-column RET 78.

\documentclass[presentation]{beamer}
\input{central-limit-thm-preamble.tex}

\author{
  Omid Khanmohamadi (\texttt{okhanmoh at math.fsu.edu})\\
  Diego Hern\'{a}n D\'{i}az Mart\'{i}nez (\texttt{ddiazmar at math.fsu.edu})\\
  Tony Wills (\texttt{twills at math.fsu.edu})\\
  Kouadio David Yao (\texttt{kyao at math.fsu.edu})\\
}

\setbeamerfont{institute}{size=\Large}
\institute[]{
  SIAM Chapter\\
  Florida State University
}

\date{2014-03-17}
\title{A Brief Analysis of Central Limit Theorem}

\begin{document}

\maketitle
\begin{frame}{Outline}
\tableofcontents
\end{frame}

% Omid
\section{Examples}

\begin{frame}[label=ex-quotes]{From Concrete to Abstract: Examples
    then Theorems!}

\begin{columns}
\begin{column}{.3\textwidth}
\includegraphics[width=.9\linewidth]{./pictures/halmos}
\end{column}

\begin{column}{.7\textwidth}
\begin{quotation} %%
The source of all great mathematics is the special case, the concrete
example. It is frequent in mathematics that every instance of a
concept of seemingly great generality is in essence the same as a
small and concrete special case.\\
--Paul Halmos (1916--2006)

\creditto[https://en.wikipedia.org/wiki/Paul_Halmos]{image source: Wikipedia}

\end{quotation}

\end{column}
\end{columns}

\begin{columns}
\begin{column}{.7\textwidth}
\begin{quotation} %%
  You should start with understanding the interesting examples and
  build up to explain what the general phenomena are. This was your
  progress from initial understanding to more understanding.\\
  -- Michael Atiyah

  \creditto[https://en.wikipedia.org/wiki/Michael_Atiyah]{image
    source: Wikipedia}

\end{quotation}
\end{column}

\begin{column}{.3\textwidth}
\includegraphics[width=.8\linewidth]{./pictures/atiyah}
\end{column}
\end{columns}

\end{frame}

\label{ex}
\begin{frame}[label=ex-dice]{Sum of Dice Throws is (Eventually)
    Normally Distributed}

  \begin{columns}
    \begin{column}{.3\textwidth}
      Comparison of probability density functions, $p(k)$, for
      \emph{sum} of $n$ fair $6$-sided dice, showing convergence to a
      normal distribution with increasing $n$

      \creditto[http://en.wikipedia.org/wiki/Gaussian_distribution]{image
        source: Wikipedia}
    \end{column}

    \begin{column}{.7\textwidth}
      \includegraphics[width=.9\linewidth]{./pictures/dice_clt}
    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[label=ex-dice-contd]{Dice Throws (Cont'd)}

  \begin{itemize}
    \setlength{\itemsep}{20pt}
  \item Roll a fair dice $10^{9}$ times, with each roll independently
    of others.
    \begin{itemize}
    \item fair = faces have equal probability (identically distributed)
    \end{itemize}
  \item Let $X_{i}$ be the number that come up on the $i$th die and
    let $S_{10^{9}}=\sum_{i=1}^{10^{9}} X_{i}$ be the total (sum) of the
    numbers rolled.
  \item The probability that $S_{10^{9}}$ is less than $x$ standard
    deviations above its mean is (approximately)
    $\frac{1}{\sqrt{2\pi}} \int_{-\infty}^{x} e^{-t^{2}/2} \,dt$.
  \end{itemize}

\end{frame}

\section{Statement of Theorem}
\label{clt}
\begin{frame}[label=prf-def-asmptn]{Definitions and Assumptions}
  Let $X_{1}, X_{2}, \dots, X_{n}$ be a sequence i.i.d random
  variables, each with mean $\mu=0$ and variance $\sigma^{2}=1$. Let
  $S_{n}=\sum_{i=1}^{n}X_{i}$.
  \begin{itemize}
    \setlength{\itemsep}{20pt}
  \item Any other \emph{finite} $\mu$ and $\sigma^{2}$ may be
    reduced to this case.
  \end{itemize}
  \begin{itemize}
  \item
    $\mathbb{E}\left[ \dfrac{S_{n}}{\sqrt{n}} \right]
    =
    \dfrac{1}{\sqrt{n}} \mathbb{E}[S_{n}]
    =
    \dfrac{1}{\sqrt{n}} \sum_{i=1}^{n}\mathbb{E}[X_{i}]
    =
    0.$
    \begin{itemize}
    \item Mean ($\mathbb{E}$) is a linear function.
    \end{itemize}
  \item
    $\Var\left[ \dfrac{S_{n}}{\sqrt{n}} \right]
    =
    \left( \dfrac{1}{\sqrt{n}} \right)^{2} \Var[S_{n}]
    =
    \dfrac{1}{n} \sum_{i=1}^{n}\Var[X_{i}]
    =
    \dfrac{1}{n} n
    =
    1.$
    \begin{itemize}
    \item $\Var$ is not a linear function; it distributes over sums
      (when the random variables are independent) and it squares
      scalar multipliers.
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}[label=prf-def-asmptn]{Definitions and Assumptions (cont'd)}
Central Limit Theorem is a statement about the so-called
    \emph{normalized} sum defined as
    $\frac{S_{n}-n\mu}{\sqrt{n}\sigma}$ which in our case is
    $\frac{S_{n}}{\sqrt{n}}$.
    \begin{itemize}
    \item Normalized mean is the difference between the sum $S_{n}$
      and its expected value $n\mu$, measured relative to (``in units
      of'') standard deviation $\sqrt{n}\sigma$; it measures how many
      standard deviations the sum is from its expected value.
    \end{itemize}
\end{frame}

\begin{frame}[label=thm-state]{Statement of Central Limit Theorem}
  With the assumptions of the previous slide, we have
  \[
  \Pr\left( a \le \frac{S_{n}}{\sqrt{n}} \le b \right) \to
  \frac{1}{\sqrt{2\pi}} \int_{a}^{b} e^{-t^{2}/2} \,dt
  \quad \text{as $n \to \infty$}
  \]
  Convergence ($\to$) is in \emph{distribution}.
  \begin{itemize}
  \item Convergence is \emph{not} in probability or almost surely.
  \item Convergence is \emph{not} uniform.
    \begin{itemize}
    \item Tails of the distribution converge more slowly than its center.
    \end{itemize}
  \end{itemize}
\end{frame}

% David
\section{Modes of Convergence}
\label{converge}

\begin{frame}[label=convergence-distr]{Convergence in Distribution}
  Central Limit Theorem is expressed in terms of ``convergence in
  distribution'' which is defined as follows:
  \begin{definition}[Convergence in Distribution]
    A sequence of random variables $X_1,\dots, X_n$ converges in
    distribution to $X$ if,
    \[
    F_{X_{n}}(x) \to F_{X}(x) \quad \text{as $n \to \infty$}
    \]
    at all points $x$ where $F_{X}$ is continuous, where $F_{X}$
    represents the distribution of the random variable $X$, given
    by $F_{X}(x):=\Pr(X \le x)$
  \end{definition}
\end{frame}

\begin{frame}[label=characteristic]{Characteristic Function and its
    relation to Convergence in Distribution}
  \begin{definition}[Characteristic function]
    The characteristic function of any real-valued random variable
    completely defines its probability distribution. Let $F_{X}$ be
    the distribution function of the random variable $X$, the
    characteristic function of $X$ is the function $\phi_{X}$ given by
    \[
    \mathbb{E}[ e^{i \xi X}]=\phi_{X}(\xi)= \int_{-\infty}^{\infty} e^{i \xi x}\, \mathrm{d}F_{X}(x)=
    \int_{-\infty}^{\infty} f_{X}(x) e^{i \xi x} \, \mathrm{d}x,
    \]
    where $f_{X}$ is the density function of $X$ (if it exists).
\end{definition}
% Add the expectation
\begin{itemize}
\item Notice the relation to Fourier transform if the density $f_{X}$ exists.
\item Convergence in distribution and convergence in characteristic
  are equivalent.
\end{itemize}
\end{frame}

% Tony
\section{Fourier Transform and Convolution}
\label{fourier}
\begin{frame}[label=fourier]{Fourier Transform Pair}
The convention we will be using is that the (1 dimensional) Fourier transform of a function $f(x)$ is
\begin{equation*}
\widehat{f}(\xi) = \int_{-\infty}^\infty f(x) e^{i \xi x} \,\mathrm{d}x
\end{equation*}
and the inverse Fourier transform of a function $\widehat{f}(\xi)$ is
\begin{equation*}
f(x) = \frac{1}{2 \pi} \int_{-\infty}^\infty \widehat{f}(\xi) e^{-i \xi x} \,\mathrm{d}\xi.
\end{equation*}
\end{frame}

\begin{frame}[label=convolution]{Convolution}
If $f$ and $g$ are integrable functions, we define the convolution $f \star g$ by
    \begin{equation*}
    (f \star g)(x) = \int_{-\infty}^\infty f(x-y) g(y) \,\mathrm{d}y.
    \end{equation*}
    \begin{itemize}
    \item Convolution is sometimes also known by its German name,
      faltung ("folding"). Later, in the proof section, we see
      ``$n$-fold convolution'' which means ``convolution repeated $n$
      times''.
    \end{itemize}
\end{frame}

\begin{frame}[label=properties]{Basic Properties of Fourier Transform}
There are a few basic properties of the Fourier transform that we will need to know. In particular, we need to know what the Fourier transform does to scaling, a Gaussian distribution, and convolution.
\begin{itemize}
  \item Scaling: For a non-zero real number $\alpha$, if $g(x) = f(\alpha x)$, then
    \begin{equation*}
    \widehat{g}(\xi) = \frac{1}{|\alpha|} \widehat{f}\left(\frac{\xi}{\alpha}\right).
    \end{equation*}
  \item Gaussian: If $f(x) = \frac{1}{\sqrt{2\pi}} e^{-\frac{x^2}{2}}$, then
    \begin{equation*}
    \widehat{f}(\xi) = \sqrt{2\pi} f(\xi)
    \end{equation*}
  \item Convolution: Under Fourier transforms the convolution becomes multiplication.
    \begin{equation*}
    \widehat{(f \star g)}(\xi) = \widehat{f}(\xi) \widehat{g}(\xi)
    \end{equation*}
\end{itemize}
\end{frame}

% All
\section{Outline of Proof}
\label{proof-outline}
\begin{frame}[label=halmos-tell-them]{Overview, View, Review!}

  \begin{columns}

    \begin{column}{.6\textwidth}
      \begin{quotation}
      Tell them what you're going to tell them, tell them, and tell
      them what you told them.\\
      --Paul Halmos (1916--2006)
      \creditto[https://en.wikipedia.org/wiki/Paul_Halmos]{image
        source: Wikipedia}
      \end{quotation}
    \end{column}

    \begin{column}{.4\textwidth}
      \includegraphics[width=.9\linewidth]{./pictures/halmos}
    \end{column}

  \end{columns}

\end{frame}

\begin{frame}[label=proof-overview]{An Overview of the Outline of the
    Proof}
Our goal is to outline the steps in showing:
\begin{equation*}
\Pr\left( a \le \frac{S_{n}}{\sqrt{n}} \le b \right) \to
  \frac{1}{\sqrt{2\pi}} \int_{a}^{b} e^{-t^{2}/2} \,\mathrm{d}t
\end{equation*}
\vspace{-0.5cm}
  \begin{enumerate}
  \item Write density of sum $S_{n}$ in terms of density of its i.i.d
    terms $X_{i}$ (by using an $n$-fold convolution) to go from $f$ to
    $f_{S_{n}}$.
  \item Find effect of scaling on density (by using a substitution in
    the integral) to go from $f_{S_{n}}$ to $f_{S_{n}/\sqrt{n}}$.
  \item Use the scaling results for Fourier transform and density as
    well as convolution to go from $f_{S_{n}/\sqrt{n}}$ to
    $\widehat{f_{S_{n}/\sqrt{n}}}$.
  \item Expand $\widehat{f}$ around zero to find a ``useful''
    converging expression.
  \item Rewrite that converging expression for
    $\widehat{f_{S_{n}/\sqrt{n}}}$ to get convergence to a Gaussian
    density
  \item Take inverse Fourier transform to arrive at the standard
    Gaussian density.
  \end{enumerate}
\end{frame}

\begin{frame}[label=f-to-f-Sn]{\step: From $f$ to $f_{S_{n}}$:
    $n$-Fold Convolution}
  We show the result for two iid variables, $X_{1}$ and $X_{2}$, with
  identical distributions $F_{X_{1}} \equiv F_{X_{2}}=:F$ and
  densities $f_{X_{1}} \equiv f_{X_{2}} =:f$.
  \begin{itemize}
  \item $f_{X_{1}+X_{2}}(a)
    =
    \frac{d}{da}F_{X_{1}+X_{2}}(a)
    =
    \frac{d}{da}\Pr\{X_{1}+X_{2} \le a\}$.
  \item $F_{X_{1}+X_{2}}(a)$ is given by the integral over
    $\{(x_{1},x_{2}) \colon x_{1}+x_{2} \le a\}$ of
    $
    f_{X_{1}}(x_{1})f_{X_{2}}(x_{2})
    =
    f(x_{1})f(x_{2})$:
    \begin{align*}
      F_{X_{1}+X_{2}}(a)=\Pr\{X_{1}+X_{2} \le a\}
      &=
      \int_{-\infty}^{\infty} \int_{-\infty}^{a-x_{2}}
      f(x_{1})f(x_{2}) \,dx_{1}dx_{2} \\
      &=
      \int_{-\infty}^{\infty} F(a-x)f(x)\,dx
    \end{align*}
  \end{itemize}
  Differentiation gives
  \[
  f_{X_{1}+X_{2}}(a)=
  \frac{d}{da} \int_{-\infty}^{\infty} F(a-x)f(x)\,dx =
  \int_{-\infty}^{\infty} f(a-x)f(x)\,dx=
  f \star f(a)
  \]

\end{frame}

\begin{frame}[label=f-Sn-to-f-Sn-normal]{\step: From $f_{S_{n}}$ to
    $f_{S_{n}/\sqrt{n}}$: Effect of Scaling on Density}
  The Central Limit Theorem involves the probability
\begin{equation*}
\Pr\left( a \le \frac{S_{n}}{\sqrt{n}} \le b \right).
\end{equation*}
Notice that if the density of $S_{n}$ is $f_{S_{n}}(t)$, then
\begin{align*}
  \Pr\left( a \le \frac{S_{n}}{\sqrt{n}} \le b \right)
  &= \Pr\left( a\sqrt{n} \le S_{n} \le b\sqrt{n} \right) \\
  &= \int_{a\sqrt{n}}^{b\sqrt{n}} f_{S_{n}}(t) \,\mathrm{d}t \\
  &= \int_a^b \sqrt{n}f_{S_{n}}(\sqrt{n}s) \,\mathrm{d}s
\end{align*}
by making the substitution $s = \frac{t}{\sqrt{n}}$. This shows that
the density of $\frac{S_{n}}{\sqrt{n}}$ is
$\sqrt{n}f_{S_{n}}(\sqrt{n}t)$.
\end{frame}

\begin{frame}[label=f-Sn-normal-to-f-hat-Sn-normal]{\step: From
    $f_{S_{n}/\sqrt{n}}$ to $\widehat{f}_{S_{n}/\sqrt{n}}$}
  Now, we have everything we need to get from the density $f$ of a
  sequence of i.i.d random variables to the characteristic
  $\widehat{f_{S_{n}/\sqrt{n}}}(\xi)$ of the corresponding normalized
  sum $S_{n}/\sqrt{n}$:
\begin{itemize}
  \setlength{\itemsep}{20pt}
\item $f_{S_{n}}(t)=f \star \cdots \star f(t)$.
\item $
  \widehat{f_{S_{n}}}(\xi)=
  \widehat{(f \star \cdots \star f)}(\xi)=
  (\widehat{f})^{n}(\xi)$
\item $f_{S_{n}/\sqrt{n}}(t)=\sqrt{n}f_{S_{n}}(\sqrt{n}t)$.
\end{itemize}

\begin{align*}
  \widehat{f_{S_{n}/\sqrt{n}}}(\xi)
  &=
  \sqrt{n}\widehat{f_{S_{n}}(\sqrt{n}t)}(\xi)
  =
  \sqrt{n}\frac{1}{\sqrt{n}}\widehat{f_{S_{n}}}\left(\frac{\xi}{\sqrt{n}}\right)\\
  &=
  \widehat{f_{S_{n}}}\left(\frac{\xi}{\sqrt{n}}\right)
  =
  (\widehat{f})^{n}\left(\frac{\xi}{\sqrt{n}}\right)
\end{align*}
\end{frame}

\begin{frame}[label=taylor-f]{\step: Taylor Expansion of $\widehat{f}$
    at $0$}
  The Fourier Transform of the density $f$ (identical for all) of
  $X_{i}$ is
  \[
  \widehat{f}(\xi)=\int_{-\infty}^{\infty}e^{i \xi x}f(x)dx
  \]

Differentiation under the integral sign can be done, so the Taylor Series is
\begin{align*}
  \widehat{f}(\xi)
  &=
  \widehat{f}(0)
  +
  \widehat{f}'(0)\xi
  +
  \frac{\widehat{f}''(0)\xi^2 }{2}
  +
  \epsilon(\xi)\xi^2
\end{align*}
as $\xi \to 0$, in which limit $\epsilon(\xi)\to 0 $ also. Observe that
\begin{itemize}
\item $\widehat{f}(0)=\int_{-\infty}^{\infty}f(x)dx=1$
\item $\widehat{f}'(0)=i\int_{-\infty}^{\infty}xf(x)dx=0$ \hspace{0.5in} (mean 0)
\item $\widehat{f}''(0)=-\int_{-\infty}^{\infty}x^2 f(x)dx=-1$ \hspace{0.2in} (variance 1)
\end{itemize}
\end{frame}

\begin{frame}[label=taylor-f-contd]{Taylor Expansion of $\widehat{f}$
    at $0$ (cont'd)}
  So
\begin{align*}
\widehat{f}(\xi) = 1-\frac{\xi^2}{2}+\epsilon(\xi)\xi^2
\end{align*}

as $\xi \to 0$, which is the same as

\begin{align*}
\xi^{-2}\left \vert \widehat{f}(\xi) - \left(1-\frac{\xi^2}{2}\right)\right \vert\to 0
\end{align*}
as $\xi \to 0$.
\end{frame}

\begin{frame}[label=conv-f-Sn-normal-to-gaussian]{\step: Convergence
    of $\widehat{f}_{S_n/\sqrt{n}}(\xi)$ to $e^{-\xi^{2}/2}$}
  Hoping that we may get a similar convergence result for
  $\widehat{f}_{S_{n}/\sqrt{n}}$, we write

\begin{align*}
  \left \vert
    (\widehat{f})^n(\xi/\sqrt{n})
    -
    \left(1-\frac{\xi^2}{2n}\right)^n
  \right \vert
\end{align*}

\begin{align*}
  =
  \left \vert
    \widehat{f}(\xi/\sqrt{n})
    -
    \left(1-\frac{\xi^2}{2n}\right)
  \right \vert
  \left\vert\sum_{k=0}^{n-1}(\widehat{f})^k(\xi/\sqrt{n})
    \left(1-\frac{\xi^2}{2n}\right)^{n-k-1}
  \right\vert
\end{align*}

\begin{align*}
 \leq \left \vert \widehat{f}(\xi/\sqrt{n})- \left(1-\frac{\xi^2}{2n}\right)\right \vert \sum_{k=0}^{n-1} \left\vert \widehat{f}(\xi/\sqrt{n}) \right\vert^k  \left\vert 1-\frac{\xi^2}{2n}\right\vert ^{n-k-1}
\end{align*}


\end{frame}

\begin{frame}[label=conv-f-Sn-normal-to-gaussian-contd]{Convergence of
    $\widehat{f}_{S_n/\sqrt{n}}(\xi)$ to $e^{-\xi^{2}/2}$}
 Since $\vert \widehat{f}(\xi)\vert\leq\Vert \widehat{f}\Vert_{\mathcal{L}^{\infty}}\leq \Vert f \Vert_{\mathcal{L}^{1}}=1$, for $n$ large enough we have

\begin{align*}
  \left \vert
    (\widehat{f})^n(\xi/\sqrt{n})
    -
    \left(1-\frac{\xi^2}{2n}\right)^n
  \right \vert
  \leq
  n
  \left \vert
    \widehat{f} (\xi/\sqrt{n})
    - \left(1-\frac{\xi^2}{2n}\right)
  \right \vert
\end{align*}

It's clear that as $n\to \infty$, $\xi/\sqrt{n}\to 0$, so

\begin{align*}
  \left \vert
    (\widehat{f})^n(\xi/\sqrt{n})
    -
    \left(1-\frac{\xi^2}{2n}\right)^n
  \right \vert \to 0
\end{align*}

as $n\to \infty$, so

\begin{align*}
  \widehat{f_{S_{n}/\sqrt{n}}}(\xi)
  =
  (\widehat{f})^n(\xi/\sqrt{n})
  \to
  e^{-\xi^2/2}
\end{align*}
\end{frame}

\begin{frame}[label=inv-fourier]{\step: Convergence of
    $f_{S_{n}/\sqrt{n}}(x)$ to $e^{-x^{2}/2}/\sqrt{2\pi}$: Inverse
    Fourier Transform}

Taking the inverse Fourier Transform we obtain

\begin{align*}
f_{S_n/\sqrt{n}}(x) \to \frac{1}{\sqrt{2\pi}}e^{-x^2/2}
\end{align*}

as $n\to \infty$, which is the conclusion of the Central Limit Theorem!

\begin{itemize}
\item Observe that this is pointwise convergence in density (or
  equivalently in distribution).
\end{itemize}

\end{frame}

% Diego
\section{Generalizations}
\label{gen}
\begin{frame}[label=gen-dirs]{Directions for Generalization}
Three general versions of CLT will be discussed:
\begin{itemize}
  \setlength{\itemsep}{20pt}
        \item Lyapunov's CLT which weakens the hypothesis of identical distribution with a tradeback on the hypothesis of finite variance (Lyapunov's Condition).
        \item Lindeberg's CLT which weakens Lyapunov's Condition (finite variance) and maintains the same weak requirements on the distribution of the random variables.
        \item Multivariate CLT which uses the covariance matrix of the random variables for the generalization.
\end{itemize}
\end{frame}

\begin{frame}[label=gen-dirs]{Lyapunov's CLT}

Suppose ${X_1, X_2, \dots, X_n}$ is a sequence of independent random variables, each with finite expected value $\mu_i$ and variance $\sigma_i^2$ (i.e. not identically distributed). Let

\begin{equation*}
        s_n^2=\sum_{i=1}^n \sigma_i^2
\end{equation*}

and for some $\delta>0$, the following condition (called Lyapunov condition), holds

\begin{equation*}
        \lim_{n\to \infty}\frac{1}{s_n^{2+\delta}}\sum_{i=1}^n E\left[\vert X_i-\mu_i\vert^{2+\delta}\right]=0
\end{equation*}

then a sum of $\frac{X_i - \mu_i}{s_n}$ converges in distribution to a standard normal random variable, as $n\to \infty $.

\end{frame}

\begin{frame}[label=gen-dirs]{Lindeberg's CLT}

Suppose ${X_1, X_2, \dots, X_n}$ is a sequence of independent random variables, each with finite expected value $\mu_i$ and variance $\sigma_i^2$ (i.e. not identically distributed). Let

\begin{equation*}
        s_n^2=\sum_{i=1}^n \sigma_i^2
\end{equation*}

and for every $\epsilon>0$, the following condition (called Lindeberg condition), holds

\begin{equation*}
        \lim_{n\to \infty}\frac{1}{s_n^{2}}\sum_{i=1}^n E\left[(X_i-\mu_i)^{2}\cdot \mathbf{1}_{\{\vert X_i-\mu_i\vert>\epsilon s_n\}}\right]=0
\end{equation*}

then a sum of $\frac{X_i - \mu_i}{s_n}$ converges in distribution to a standard normal random variable, as $n\to \infty $.

\end{frame}

\begin{frame}[label=gen-cmp-conds]{Comparison of Finite Variance Conditions}

Lindeberg:
\begin{equation*}
\int_{\vert X_i-\mu_i\vert>\epsilon s_n }(X_i-\mu_i)^{2} d f_i<\infty
\end{equation*}
Classical:
\begin{equation*}
\int_{\mathbb{R} }(X_i-\mu_i)^{2} d f_i <\infty
\end{equation*}
Lyapunov:
\begin{equation*}
\int_{\mathbb{R} }\vert X_i-\mu_i\vert ^{2+\delta} d f_i<\infty
\end{equation*}

Observe that, in the Classical CLT, $\mu_i=\mu$ and $f_i(x)=f(x)$ $\forall i$

\end{frame}

\begin{frame}[label=gen-nutshell]{Generalizations in a Nutshell: CLT
    is Robust}

  \begin{itemize}
  \item If one has a lot of ``small'' random terms which are ``mostly
    independent'' and each contributes a ``small fraction'' of the
    total sum, then the total sum must be ``approximately'' normally
    distributed.
  \end{itemize}

\end{frame}

\begin{frame}[label=gen-nD]{Multivariate CLT}

Suppose $\{X_1, X_2, \dots, X_n\}\in \mathbb{R}^d$ is a sequence of independent random vectors ,  with finite mean vector $E[X_i]=\mu$ and finite covariance matrix $\Sigma$, then

\begin{equation*}
\frac{1}{\sqrt{n}}\left(\sum_{i=1}^nX_i-n\mu\right)\longrightarrow \mathcal{N}_d(\mathbf{0},\Sigma)
\end{equation*}

in distribution as $n\to \infty $, where ${N}_d(\mathbf{0},\Sigma)$ is the multivariate normal distribution with mean vector $\mathbf{0}$ and covariance matrix $\Sigma$. \vskip 0.5 truecm

Note: Addition is done componentwise.
\end{frame}

\begin{frame}[label=Thank You]{Thank you for your attention!}

        \begin{figure}
        \includegraphics[scale=0.04]{./pictures/laplace}
        \caption{Laplace $\triangle$}
        \end{figure}

\end{frame}

\appendix

\section{More Details}
\label{details}
\begin{frame}[label=(a.s.)-probability]{Almost Sure convergence and Convergence in Probability}

  Because of their relationship to Convergence in Distribution, it is
  useful to review { \bf \em Almost Sure Convergence } and {\bf \em
    Convergence in Probability.} We let $X_1, X_2, \dots , X_n,\dots$
  be a sequence of random variables defined on the probability space
  $(\Omega, \mathcal{F}, P)$

\begin{itemize}
\item {\bf Almost Sure Convergence} (Strong convergence):\\
$X_1, X_2, \dots, X_n,\dots$ converges almost surely to a random
variable $X$ if, for every $\varepsilon > 0$
\[
P\left(  \lim_{n \to \infty} |X_n - X| < \varepsilon \right)=1
\]
\item  {\bf Convergence in Probability} (Weak convergence):\\
  $X_1, X_2, \dots, X_n,\dots$ converges in probability to $X$ if, for
  for every $\varepsilon > 0$
\[
\lim_{n \to \infty} P\left(  |X_n - X| < \varepsilon \right)=
1~\text{or}~ \lim_{n \to \infty} P\left(  |X_n - X| \geq
  \varepsilon \right)= 0
\]
\end{itemize}

\end{frame}

\begin{frame}[label=ImportantRelationships]{Notable Relationship between Convergence Concepts}
\Large  (A.S.) Conv $\implies$ Conv in Prob $\implies$ Conv in Distribution
\end{frame}

\begin{frame}[label=Discussion]{Some Unanswered Questions}
\begin{itemize}
\item Recall the expression of the characteristic function of a random variable $X$ was given by
\begin{center}
$  \int_{-\infty}^{\infty} e^{i \xi x}\, \mathrm{d}F_{X}(x)=
    \int_{-\infty}^{\infty} f_{X}(x) e^{i \xi x} \, \mathrm{d}x,$
\end{center}
and the expression on the right-hand side will hold only if $f$ the density of the random variable exists.
\item One of the questions I would like for us to be able to answer in more detail is, In which case do we not have a density?, and more precisely, Does a discrete random variable have a distribution that is absolutely continuous with respect to the Lebesgue measure on $\mathbb{R}$?
\end{itemize}



\end{frame}

\begin{frame}[label= Exploratory]{Exploratory Questions }
\begin{itemize}
\item For this part, I would like for the discussion to focus more on the properties of the characteristic function in a more general sense.
\item It would be nice to have a clear idea about the strenght of using the characteristic function.(I will give more details about that comment soon)
\end{itemize}

\end{frame}


\end{document}
